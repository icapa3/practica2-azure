########################################
#           Fichero principal          #
########################################

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.1"
    }
  }
}



########################################
#         Grupo de recursos            #  
########################################
# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group

resource "azurerm_resource_group" "k8s_israel_resources_group" {
    name     =  "k8sisraelresourcesgroup"
    location = var.location

    tags = {
        environment = "CP2"
    }

}

########################################
#            Storage Account           #  
########################################
# Storage account
# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/storage_account

resource "azurerm_storage_account" "k8s_israel_storage_account" {
  name                     = var.storage_account
  resource_group_name      = azurerm_resource_group.k8s_israel_resources_group.name
  location                 = azurerm_resource_group.k8s_israel_resources_group.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "CP2"
  }
}