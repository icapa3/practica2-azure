#########################################
#     Definición Máquinas Virtuales     #
#########################################
# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/linux_virtual_machine 

resource "azurerm_linux_virtual_machine" "k8s_israel_maquina_virtual" {
    count				= length(var.machines)
    name                = "k8sisraelvirtual-${var.machines[count.index]}"
    resource_group_name = azurerm_resource_group.k8s_israel_resources_group.name
    location            = azurerm_resource_group.k8s_israel_resources_group.location
    size                = var.machines[count.index] != "kubemaster" ? var.vm_size : var.vm_size_master
    admin_username      = var.ssh_user
    network_interface_ids = [ azurerm_network_interface.k8s_israel_machine_nic[count.index].id ]
    disable_password_authentication = true

    admin_ssh_key {
        username   = var.ssh_user
        public_key = file(var.public_key_path)
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "Standard_LRS"
    }

    plan {
        name      = "centos-8-stream-free"
        product   = "centos-8-stream-free"
        publisher = "cognosys"
    }

    source_image_reference {
        publisher = "cognosys"
        offer     = "centos-8-stream-free"
        sku       = "centos-8-stream-free"
        version   = "1.2019.0810"
    }

    boot_diagnostics {
        storage_account_uri = azurerm_storage_account.k8s_israel_storage_account.primary_blob_endpoint
    }

    tags = {
        environment = "CP2"
    }

########################################
#         Instalación Software         #  
########################################

 # Copia de la clave privada
  provisioner "file" {
    source      = var.private_key_path
    destination = "/tmp/id_rsa"

    connection {
      host     = self.public_ip_address
      user     = self.admin_username
      private_key = file(var.private_key_path)
      agent	   = true
    }    
  }

 # Copia de la clave publica
  provisioner "file" {
    source      = var.public_key_path
    destination = "/tmp/id_rsa.pub"

    connection {
      host     = self.public_ip_address
      user     = self.admin_username
      private_key = file(var.private_key_path)
      agent	   = true
    }    
  }  

  provisioner "file" {
    source      = "config/config-all.sh"
    destination = "/tmp/config-all.sh"

    connection {
      host     = self.public_ip_address
      user     = self.admin_username
      private_key = file(var.private_key_path)
      agent	   = true
    }    
  }  

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/config-all.sh",
      "sudo /tmp/config-all.sh",
    ]
    
    connection {
      host     = self.public_ip_address
      user     = self.admin_username
      private_key = file(var.private_key_path)
      agent	   = true
    }
  } 
}
