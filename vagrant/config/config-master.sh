#!/bin/bash

sudo dnf install git-all epel-release -y
sudo -u ansible pip3.8 install ansible --user
# Instalamos sshpass para poder autorizar a los nodos de forma no interactiva
sudo dnf install sshpass git tree jq -y
sudo dnf update -y
# Generamos la clave rsa para el usuario ansible
sudo -u ansible < /dev/zero ssh-keygen -t rsa -b 4096 -q -N "" 
# Copiamos las claves rsa para poder conectarnos remotamente a las máquinas (Master, worker1, worker2 y nfs)
sudo -u ansible sshpass -p "ansible01#" ssh-copy-id -o StrictHostKeyChecking=no ansible@192.167.1.110 -f
sudo -u ansible sshpass -p "ansible01#" ssh-copy-id -o StrictHostKeyChecking=no ansible@192.167.1.111 -f
sudo -u ansible sshpass -p "ansible01#" ssh-copy-id -o StrictHostKeyChecking=no ansible@192.167.1.112 -f
sudo -u ansible sshpass -p "ansible01#" ssh-copy-id -o StrictHostKeyChecking=no ansible@192.167.1.113 -f
sudo -u ansible git config --global credential.helper store
# sudo reboot