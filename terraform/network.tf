#################################
# 		Creación de la red		#
#################################
# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_network


resource "azurerm_virtual_network" "k8s_israel_network" {
    name                = "k8sisraelkubernetesnet"
    address_space       = ["192.167.0.0/16"]
    location            = azurerm_resource_group.k8s_israel_resources_group.location
    resource_group_name = azurerm_resource_group.k8s_israel_resources_group.name

    tags = {
        environment = "CP2"
    }
}

#################################
#      Creación de la subnet    #
#################################
# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subnet

resource "azurerm_subnet" "k8s_israel_subnet" {
    name                   = "k8sisraelsubnet"
    resource_group_name    = azurerm_resource_group.k8s_israel_resources_group.name
    virtual_network_name   = azurerm_virtual_network.k8s_israel_network.name
    address_prefixes       = ["192.167.1.0/24"]

}


#################################
#    Creación tarjeta de red    #
#################################

# Create NIC
# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/network_interface

# NIC servidor Master
resource "azurerm_network_interface" "k8s_israel_machine_nic" {
  count			      = length(var.machines)
  name                = "k8sisrael${var.machines[count.index]}nic"  
  location            = azurerm_resource_group.k8s_israel_resources_group.location
  resource_group_name = azurerm_resource_group.k8s_israel_resources_group.name

    ip_configuration {
    name                           = "k8sisraelip${var.machines[count.index]}"
    subnet_id                      = azurerm_subnet.k8s_israel_subnet.id 
    private_ip_address_allocation  = "Static"
    private_ip_address             = "192.167.1.${count.index + 110}"
    public_ip_address_id           = azurerm_public_ip.k8s_israel_public_ip[count.index].id
  }

    tags = {
        environment = "CP2"
    }

}

########################################
#      Creación de las IPs Públicas    #
########################################
# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/public_ip

# IP Pública para el Máster
resource "azurerm_public_ip" "k8s_israel_public_ip" {
  count			      = length(var.machines)
  name                = "k8sisraelpublicip${var.machines[count.index]}"
  location            = azurerm_resource_group.k8s_israel_resources_group.location
  resource_group_name = azurerm_resource_group.k8s_israel_resources_group.name
  allocation_method   = "Dynamic"
  sku                 = "Basic"

    tags = {
        environment = "CP2"
    }
}