# Automatización Terraform, Vagrant y Azure

Con este repositorio se podrá desplegar un entorno Kubernetes, tanto en un laboratorio local con máquinas VirtualBox y, automatizada la creación con Vagrant y en un cloud como Azure pero, en este caso, automatizando la creación de la infraestructura con Terraform.

<br />
En la carpeta terraform se encuentra todo el código necesario para la automatización de la infraestructura en azure. Para ello, se deberá crear el fichero **credentials.tf** y **corrections-vars.tf** donde incluir las partes asociadas a Azure como:

*credentials.tf*

	provider "azurerm" {
	  features {}
	  subscription_id = ""
	  client_id       = ""
	  client_secret   = ""
	  tenant_id       = ""
	}

*corrections-vars.tf*
¡IMPORTANTE! Debido a que los planes de Terraform también realizan una instalación mínima del software a utilizar, es imprescindible indicar tanto la ruta de la clave pública como de la clave privada.

	variable "location" {
	  type = string
	  description = "Región de Azure donde crearemos la infraestructura"
	  default = "West Europe" 
	}

	variable "storage_account" {
	  type = string
	  description = "Nombre para la storage account"
	  default = "k8sisraelstorageaccount"
	}

	variable "public_key_path" {
	  type = string
	  description = "Ruta para la clave pública de acceso a las instancias"
	  default = "ssh/id_rsa.pub" # o la ruta correspondiente
	}

	variable "private_key_path" {
	  type = string
	  description = "Ruta para la clave pública de acceso a las instancias"
	  default = "ssh/id_rsa" # o la ruta correspondiente
	}

	variable "ssh_user" {
	  type = string
	  description = "Usuario para hacer ssh"
	  default = "ansible"
	}