################################################
# Grupo de Seguridad de Azure (Security Group) #  
################################################
# Security group
# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/network_security_group

resource "azurerm_network_security_group" "k8s_israel_security_group" {
    name                = "k8sisraelsshtraffic"
    location            = azurerm_resource_group.k8s_israel_resources_group.location
    resource_group_name = azurerm_resource_group.k8s_israel_resources_group.name

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags = {
        environment = "CP2"
    }
}

###################################################
# Vincular Security Group a las Interfaces de Red #
###################################################
# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/network_interface_security_group_association

resource "azurerm_network_interface_security_group_association" "k8s_israel_sec_group_association" {
	count				      = length(var.machines)
    network_interface_id      = azurerm_network_interface.k8s_israel_machine_nic[count.index].id
    network_security_group_id = azurerm_network_security_group.k8s_israel_security_group.id

}