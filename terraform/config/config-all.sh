#!/bin/bash

timedatectl set-timezone Europe/Madrid
dnf install python38 -y
update-alternatives --set python3 /usr/bin/python3.8
adduser -md /home/ansible ansible
echo "PS1='[${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]]\$ '" >> /etc/bashrc
# Autorizamos la escalada de privilegios para el usuario ansible
echo "ansible ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/ansible
# Añadimos las IPs de todos los nodos
echo "192.167.1.110 kubemaster.local.es" >> /etc/hosts
echo "192.167.1.111 kubenode1.local.es" >> /etc/hosts
echo "192.167.1.112 kubenode2.local.es" >> /etc/hosts
echo "192.167.1.110 nfs.local.es" >> /etc/hosts
echo "192.167.1.110 wordpress.local.es" >> /etc/hosts

# Esto solo lo tiene que realizar cuando es el master
if [[ "$HOSTNAME" = *"kubemaster"* ]]; then
	sudo -u ansible cp /tmp/id_rsa* /home/ansible/.ssh/
	sudo chmod o=-rw,g=-rw /home/ansible/.ssh/id_rsa*
	dnf install git-all epel-release -y
	sudo -u ansible pip3.8 install ansible --user
	# Instalamos sshpass para poder autorizar a los nodos de forma no interactiva
	dnf install sshpass git tree jq -y
	sudo -u ansible ssh-copy-id -o StrictHostKeyChecking=no ansible@192.167.1.110 -f
	sudo -u ansible ssh-copy-id -o StrictHostKeyChecking=no ansible@192.167.1.111 -f
	sudo -u ansible ssh-copy-id -o StrictHostKeyChecking=no ansible@192.167.1.112 -f
	sudo -u ansible git config --global credential.helper store
fi
dnf update -y

# Borramos la clave privada y pública del directorio temporal
rm /tmp/id_rsa*