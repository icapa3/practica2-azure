########################################
#         Fichero de variables         #
########################################

# Definición de la variable vm_size (Tamaño de la máquina virtual)
variable "vm_size_master" {
  type = string
  description = "Tamaño de la máquina virtual"
  default = "Standard_D2_v2" # 7 GB, 2 CPU, 100GB SSD 
}

# Definición de la variable vm_size (Tamaño de la máquina virtual)
variable "vm_size" {
  type = string
  description = "Tamaño de la máquina virtual"
  default = "Standard_D1_v2" # 3.5 GB, 1 CPU , 50GB SSD
}

# Definición de todas las máquinas
variable "machines" {
  type = list(string)
  description = "Máquinas K8s"
  default = ["kubemaster", "kubenode1", "kubenode2"]
} 