#!/bin/bash

sudo timedatectl set-timezone Europe/Madrid
sudo dnf install python38 -y
sudo update-alternatives --set python3 /usr/bin/python3.8
sudo adduser -md /home/ansible ansible
echo "ansible01#" | sudo passwd ansible --stdin
sudo echo "PS1='[${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]]\$ '" >> /etc/bashrc
# Autorizamos la escalada de privilegios para el usuario ansible
sudo echo "ansible ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/ansible
sudo yum install "kernel-devel-uname-r = $(uname -r)"
sudo yum install -y gcc perl kernel-headers kernel-devel
sudo /sbin/rcvboxadd quicksetup all
sudo dnf update -y
# Añadimos las IPs de todos los nodos
sudo echo "192.167.1.110 master.local" >> /etc/hosts
sudo echo "192.167.1.111 worker1.local" >> /etc/hosts
sudo echo "192.167.1.112 worker2.local" >> /etc/hosts
sudo echo "192.167.1.113 nfs.local" >> /etc/hosts
# sudo reboot